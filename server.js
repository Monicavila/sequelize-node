const express = require("express");
const morgan = require("morgan");
require("dotenv").config();

const {Recargas, Tarjeta} = require("./models");

const PORT = process.env.PORT;

const app = express();

//configuracion de express para poder interpretar los datos que son enviados en formato JSON
app.use(express.json()); 

app.use(morgan("dev"));

app.get('/', (req, res) => {
    res.send("Hola mundo!");
});

app.post("/tarjeta", async (req, res) => {
    const datos = req.body;
    try{
        const results = await Tarjeta.create(datos);
        res.json({message: results});
    }catch(error){
        console.log(error);
    }
});

app.post("/recarga", async (req, res) => {
    const datos = req.body;
    datos.fecha = new Date(); //Agregamos la propiedad fecha con el valor del dia de hoy
    try{
        //Creamos una recarga
        await Recargas.create(datos);

        //Obtenemos los datos de la tarjeta a la cual le queremos hacer la recarga
        const tarjeta = await Tarjeta.findOne({where: {id: datos.idTarjeta}});
        // console.log(typeof tarjeta.saldo);
        
        //Calculamos el saldo final
        let saldoFinal = Number(tarjeta.saldo) + Number(datos.valorRecarga);

        //Actualizamos el registro de la tarjeta
        const results = await Tarjeta.update({saldo: saldoFinal}, {where: {id: datos.idTarjeta}});

        //Enviamos el resultado al cliente
        res.json({message: "El saldo de la tarjeta ha sido abonado correctamente"});
    }catch(error){
        console.log(error);
    }
});

app.get("/recargas", async (req, res) => {
    try{
        const results = await Recargas.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
    }
});

app.get("/recargas/:id", async (req, res) => {
    try{
        const results = await Recargas.findOne({where: {id: req.params.id}});
        res.json(results);
    }catch(error){
        console.log(error);
    }
});

/* Reto Conductores 
* Crear tres endpoints /conductores :
* a. obtener todos los conductores
* b. obtener un conductor por id
* c. agregar un nuevo conductor
*/

app.listen(PORT, () => {
    console.log("Corriendo el servidor sobre el puerto", PORT);
});
